package clientserverconnection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class MessageStorage {
	private void createFile(String text,String fileName) {
		File file = new File(fileName); 
		boolean result;
		try {
			file.createNewFile();
			appendMessage(text,fileName);
		} catch (IOException exception) {
			exception.printStackTrace(); 
		}
	}
	private void appendMessage(String message,String fileName) {
		try {
	         File file = new File(fileName);
	         FileWriter fileWriter = new FileWriter(file.getName(),true);
	         BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
	         bufferWriter.append(' ');
	         bufferWriter.append(message);
	         bufferWriter.close();
	      } catch(IOException exception){
	    	  exception.printStackTrace();
	      }
	}
 
	public void addMessageToFile(String text,Socket socket) {
		InetAddress ip=socket.getInetAddress();
		int port=socket.getPort();
		String fileName= "C:\\"+ip + "_" + port+".txt";
		createFile(text,fileName);
	}
}