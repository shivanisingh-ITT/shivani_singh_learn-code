package clientserverconnection;

import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server {
	private void configureNewClient() throws IOException {
		ServerSocket serverSocket = new ServerSocket(5056);
		while (true) {
			Socket socket = null;
			try {
				socket = serverSocket.accept();
				System.out.println("A new client is connected : " + socket);
				DataInputStream dataInput = new DataInputStream(socket.getInputStream());
				DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());

				System.out.println("Assigning new thread for this client");
				Thread thread = new ClientHandler(socket, dataInput, dataOutput);
				thread.start();

			} catch (Exception e) {
				socket.close();
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws IOException {
		Server server = new Server();
		server.configureNewClient();
	}

	class ClientHandler extends Thread {
		DateFormat fordate = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat fortime = new SimpleDateFormat("hh:mm:ss");
		final DataInputStream dataInput;
		final DataOutputStream dataOutput;
		final Socket socket;

		public ClientHandler(Socket socket, DataInputStream dataInput, DataOutputStream dataOutput) {
			this.socket = socket;
			this.dataInput = dataInput;
			this.dataOutput = dataOutput;
		}

		@Override
		public void run() {
			String received;
			String toReturn;
			MessageStorage saveMessage=new MessageStorage();
			while (true) {
				try {
					dataOutput.writeUTF("Enter your message, enter exit to stop");
					received = dataInput.readUTF();
					if (received.equals("exit")) {
						System.out.println("Client " + this.socket + " sends exit...");
						System.out.println("Closing this connection.");
						this.socket.close();
						System.out.println("Connection closed");
						break;
					}
					Date date = new Date();
					toReturn = received + " " + fordate.format(date) + fortime.format(date);
					dataOutput.writeUTF(toReturn);
					saveMessage.addMessageToFile(toReturn,socket);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			try {
				this.dataInput.close();
				this.dataOutput.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
