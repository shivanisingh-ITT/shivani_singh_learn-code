package clientServer;

import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server {
	private void configureNewClient() throws IOException {
		ServerSocket serverSocket = new ServerSocket(5056);
		//remove static
		while (true) {
			Socket socket = null;
			try {
				socket = serverSocket.accept();
				System.out.println("A new client is connected : " + socket);
				DataInputStream dataInput = new DataInputStream(socket.getInputStream());
				DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());

				System.out.println("Assigning new thread for this client");
				Thread thread = new ClientHandler(socket, dataInput, dataOutput);
				thread.start();

			} catch (Exception e) {
				socket.close();
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws IOException {
		Server server = new Server();
		server.configureNewClient();
	}
}
