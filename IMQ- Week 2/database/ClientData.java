package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import dao.ClientDao;
import jdbcconnection.CreateStatementObject;

public class ClientData {

	public void storeClient(String Message, String clientName, String Id) {
		ClientDao client = new ClientDao();
		client.setId(Id);
		client.setClientName(clientName);
		client.setData(Message); 
		Boolean clientExists = ifClientExists(clientName); 
		if (clientExists) {
			insertClient(client);
		} else {
			createNewClient(client);
			insertClient(client);
			insertClientinRoot(client);
		}
	}

	private void insertClientinRoot(ClientDao client) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		String clientName = client.getClientName();
        String Id=client.getId();
		preparedStatement = createStatement.getPreparedStatement("insert into allclients(Id,client_name) values(?,?)");
		try {
			preparedStatement.setString(1, Id);
			preparedStatement.setString(2, clientName);
			preparedStatement.execute();
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}

	private Boolean ifClientExists(String clientName) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		preparedStatement = createStatement.getPreparedStatement(
				"SELECT Table_name as TablesName from information_schema.tables where table_name = ?;");
		try {
			preparedStatement.setString(1, clientName);
			ResultSet result = preparedStatement.executeQuery();
			result.last();
			int rows = result.getRow();
			
			if (rows == 1) {
				return true;
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
		return false;
	}

	private void insertClient(ClientDao client) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		String message = client.getData();
		String clientName = client.getClientName();  
		 preparedStatement = createStatement.getPreparedStatement("insert into "+clientName+ "(data) values(?);");
		
		try {
	 	 	 preparedStatement.setString(1, message);
			preparedStatement.execute();
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}

	private void createNewClient(ClientDao client) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		String clientName = client.getClientName();

		preparedStatement = createStatement.getPreparedStatement(
"create table "+clientName+" ( serial_no int not null auto_increment,  time timestamp not null default current_timestamp, data varchar(250),  primary key(serial_no) ); " 
				);
		try {
			preparedStatement.execute();
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}
}
