package clientServer;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Scanner;

import configFile.PropertyFile;

public class Client {
	Scanner scan = new Scanner(System.in);
	private InetAddress getIP(String ipName) {
		InetAddress ip = null;
		try {
			ip = InetAddress.getByName(ipName);
			return ip;
		} catch (UnknownHostException exception) {
			exception.printStackTrace();
		}
		return ip;
	}

	private Socket getSocket(InetAddress ip, int port) {
		Socket socket = null;
		try {
			socket = new Socket(ip, port);
			return socket;
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		return socket;
	}

	
    private void readWriteMessage(DataInputStream dataInput, DataOutputStream dataOutput, Socket socket) {
		while (true) {
			try {
				System.out.println(dataInput.readUTF());
				String toSend = scan.nextLine();
				dataOutput.writeUTF(toSend);
               
				if (toSend.equals("exit")) {
					System.out.println("Closing this connection : " + socket);
					socket.close();
					System.out.println("Connection closed");
					break;
				}
				String received = dataInput.readUTF();
				System.out.println(received);
			} catch (IOException exception) {
				exception.printStackTrace();
			}
		}
		scan.close();
		try {
			dataInput.close();
			dataOutput.close();
		} catch (IOException exception) {
			exception.printStackTrace();
		}

	}

	public static void main(String[] args) throws IOException {
		PropertyFile propertyFile=new PropertyFile();
		Properties properties=propertyFile.loadPropertiesFile();
        Client client=new Client();
		InetAddress ip = client.getIP(propertyFile.getPropertyValue("ipName",properties));
		Socket socket = client.getSocket(ip, 5056);
		DataInputStream dataInput = new DataInputStream(socket.getInputStream());
		DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
		client.readWriteMessage(dataInput, dataOutput, socket);  

	}
}
