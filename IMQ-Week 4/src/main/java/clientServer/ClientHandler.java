package clientServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import database.ClientData;

public class ClientHandler extends Thread {
	DateFormat fordate = new SimpleDateFormat("yyyy/MM/dd");
	DateFormat fortime = new SimpleDateFormat("hh:mm:ss");
	final DataInputStream dataInput;
	final DataOutputStream dataOutput;
	final Socket socket;

	public ClientHandler(Socket socket, DataInputStream dataInput, DataOutputStream dataOutput) {
		this.socket = socket;
		this.dataInput = dataInput;
		this.dataOutput = dataOutput;
	}

	@Override
	public void run() {
		String received;
		String toReturn;
		String clientName = " ";
		MessageStorage saveMessage = new MessageStorage();
       ClientData clientData=new ClientData();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter your name");
		clientName = scan.next();

		while (true) {
			try {
				dataOutput.writeUTF("Enter your message, enter exit to stop");
				received = dataInput.readUTF();
				if (received.equals("exit")) {
					System.out.println("Client " + this.socket + " sends exit...");
					System.out.println("Closing this connection.");
					this.socket.close();
					System.out.println("Connection closed");
					break;
				}
				Date date = new Date();
				toReturn = received + " " + fordate.format(date) + fortime.format(date);
				dataOutput.writeUTF(toReturn); 
				saveMessage.addMessageToFile(toReturn,socket);
				InetAddress ip=socket.getInetAddress();
				int port=socket.getPort();
				String Id=ip+"_"+port;
				clientData.storeClient(received,clientName,Id);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			this.dataInput.close();
			this.dataOutput.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

