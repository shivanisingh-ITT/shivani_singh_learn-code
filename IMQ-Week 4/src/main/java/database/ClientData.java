package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Properties;

import configFile.PropertyFile;
import dao.ClientDao;
import jdbcconnection.CreateStatementObject;

public class ClientData {
	PropertyFile propertyFile=new PropertyFile();
	Properties properties=propertyFile.loadPropertiesFile();
	
	public void storeClient(String Message, String clientName, String Id) {
		ClientDao client = new ClientDao();
		client.setId(Id);
		client.setClientName(clientName);
		client.setData(Message); 
		Boolean clientExists = ifClientExists(clientName); 
		if (clientExists) {
			insertClient(client);
		} else {
			createNewClient(client);
			insertClient(client);
			insertClientinRoot(client);
		}
	}

	private void insertClientinRoot(ClientDao client) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		String clientName = client.getClientName();
        String Id=client.getId();
		preparedStatement = createStatement.getPreparedStatement(propertyFile.getPropertyValue("insertIntoAllClients",properties));
		try {
			preparedStatement.setString(1, Id);
			preparedStatement.setString(2, clientName);
			preparedStatement.execute();
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}

	private Boolean ifClientExists(String clientName) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		preparedStatement = createStatement.getPreparedStatement(
				propertyFile.getPropertyValue("existingTable",properties));
		try {
			preparedStatement.setString(1, clientName);
			ResultSet result = preparedStatement.executeQuery();
			result.last();
			int rows = result.getRow();
			
			if (rows == 1) {
				return true;
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
		return false;
	}

	private void insertClient(ClientDao client) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		String message = client.getData();
		String clientName = client.getClientName();  
		 preparedStatement = createStatement.getPreparedStatement(propertyFile.getPropertyValue("insert",properties)+" "+clientName+ propertyFile.getPropertyValue("values",properties));
		
		try {
	 	 	 preparedStatement.setString(1, message);
			preparedStatement.execute();
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}

	private void createNewClient(ClientDao client) {
		CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
		String clientName = client.getClientName();

		preparedStatement = createStatement.getPreparedStatement(
propertyFile.getPropertyValue("createTable",properties)+" "+clientName+propertyFile.getPropertyValue("createTableColumns",properties) 
				);
		try {
			preparedStatement.execute();
		} catch (SQLException exception) {
			exception.printStackTrace();
		}
	}
}

