package dao;

public class ClientDao {
	private String Id;
	private String data;
	private String clientName;

	public void setId(String Id){
		this.Id = Id;
	}
	
	public void setData(String data) {
		this.data = data;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getId() {
		 return Id;
	 }
	
	public String getData() {
		return data;
	}

	public String getClientName() {
		return clientName;
	}

}
