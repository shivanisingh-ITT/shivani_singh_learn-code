package jdbcconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import configFile.PropertyFile;
 
public class JdbcConnection {
	PropertyFile propertyFile=new PropertyFile();
	Properties properties=propertyFile.loadPropertiesFile();
	public Connection getJdbcConnection() {  
	Connection connection=null;
		try {
			Class.forName(propertyFile.getPropertyValue("className",properties));
			String url=propertyFile.getPropertyValue("url",properties);
			String username=propertyFile.getPropertyValue("username",properties);
			String password=propertyFile.getPropertyValue("password",properties);
			connection = DriverManager.getConnection(url, username, password);
		} catch (Exception exceptionObject) {
			exceptionObject.printStackTrace();
		}
		return connection;
	}
}
