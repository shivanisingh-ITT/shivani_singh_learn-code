import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import clientTest.ClientServiceTest;
import clientTest.ParserTest;
import modeltest.ClientTest;
import modeltest.JSONMessageTest;

@RunWith(Suite.class)


@Suite.SuiteClasses({
   IMQProtocolTest.class,
   IMQRequestTest.class,
   IMQResponseTest.class,
   ClientServiceTest.class,
   ParserTest.class,
   ClientTest.class,
   JSONMessageTest.class
})

public class JunitTestSuite {
 }

 


 