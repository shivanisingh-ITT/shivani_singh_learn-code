import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import communicationProtocol.IMQRequest;
import communicationProtocol.UserCommand;

public class IMQRequestTest{

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		 IMQRequest request=new IMQRequest();
		 request.setUserCommand(UserCommand.CONFIG);
		 assertEquals(request.getUserCommand(),UserCommand.CONFIG);
	}

}
