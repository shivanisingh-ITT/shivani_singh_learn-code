package client;

import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.Scanner;

import com.google.gson.Gson;

import communicationProtocol.IMQRequest;
import communicationProtocol.UserCommand;
import model.Client;
import properties.PropertyFile;

public class ClientMain {
	private String bufferInput = null;
	private BufferedReader bufferReader = null;
	private BufferedReader socketReader = null;
	private PrintWriter printWriter = null;
	static Socket socket = null;

	public static void main(String[] args) throws IOException {
		ClientMain client = new ClientMain();
		client.connectToServer();
		client.readWriteMessage();
	}

	private InetAddress getIP(String ipName) {
		InetAddress ip = null;
		try {
			ip = InetAddress.getByName(ipName);
			return ip;
		} catch (UnknownHostException exception) {
			exception.printStackTrace();
			System.out.println("Address not found");
		}
		return ip;
	}

	private void connectToServer() throws IOException {
		PropertyFile propertyFile = new PropertyFile();
		Properties properties = propertyFile.loadPropertiesFile();
		ClientMain client = new ClientMain();
		InetAddress ip = client.getIP(propertyFile.getPropertyValue("ipName", properties));
		try {
			socket = new Socket(ip, Integer.parseInt(propertyFile.getPropertyValue("port", properties)));
			bufferReader = new BufferedReader(new InputStreamReader(System.in));
			socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			printWriter = new PrintWriter(socket.getOutputStream());
			System.out.println("Enter Command here, enter exit to stop");
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	private void readWriteMessage() throws IOException {
		String response;
		Gson gson = new Gson();
		try {
			bufferInput = bufferReader.readLine();

			// command parser to parse bufferInput
			while (!bufferInput.equalsIgnoreCase("exit")) {
				IMQRequest clientRequest = ClientService.getRequest(bufferInput, socket.getInetAddress().toString(),
						UserCommand.PUBLISH);
				String clientRequestString = gson.toJson(clientRequest);
				printWriter.println(clientRequestString);
				printWriter.flush();
				response = socketReader.readLine();
				response = response.replace('"', ' ');
				response = response.replace("response", "");
				response = response.replaceAll("[:{}]", "");

				System.out.println(response);
				bufferInput = bufferReader.readLine();
			}
		} catch (IOException exception) {
			exception.printStackTrace();
			System.out.println("Sending request to server failed!!");
		} finally {
			socketReader.close();
			printWriter.close();
			bufferReader.close();
			socket.close();
		}
	}
}
