package client;

import com.google.gson.Gson;

import model.JSONMessage;

public class Parser {
	public String getMessageType(String message) {
		Gson gson = new Gson();
		JSONMessage jsonMessage = gson.fromJson(message, JSONMessage.class);
		return jsonMessage.getMessage();
	}
}
