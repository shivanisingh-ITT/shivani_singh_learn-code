package IMQ.IMQServer; 
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import modeltest.ClientTest;
import modeltest.IMQMessageTest;
import publishersubscribertest.PublisherServiceTest;
import publishersubscribertest.SubscriberServiceTest;
import publishersubscribertest.TopicServiceTest;
import serverTest.ClientRequestTest;
import serverTest.ServerResponseTest;
import serverTest.ServerTest;
import serverTest.UserTest;

@RunWith(Suite.class)


@Suite.SuiteClasses({
   PublisherServiceTest.class,
   SubscriberServiceTest.class,
   ClientRequestTest.class,
   TopicServiceTest.class,
   ServerResponseTest.class,
   ServerTest.class,
   UserTest.class,
   ClientTest.class,
   IMQMessageTest.class
})

public class JunitTestSuite {
 }

 


 