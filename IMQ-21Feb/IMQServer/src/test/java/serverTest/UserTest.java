package serverTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Test;

import jdbcconnection.CreateStatementObject;
import server.User;

public class UserTest {
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		 CreateStatementObject createStatement = new CreateStatementObject();
			PreparedStatement preparedStatement = null;
			preparedStatement = createStatement.getPreparedStatement("delete from user where username=?");
			try { 
				preparedStatement.setString(1, "Raman");
				preparedStatement.executeUpdate(); 
			} catch (SQLException exception) {  
				exception.getMessage();
			}
	}

   User user=new User();
	@Test
	public void loginTest() throws IOException { 
		String response="Welcome Back shivani!! You have logged in sucessfully";
		String userLogin=user.login("shivani");
		assertEquals(response,userLogin);
	}

	@Test
	public void loginNewUserTest() throws IOException { 
		String response="Welcome Raman!! You have signed up sucessfully";
		String userLogin=user.login("Raman");
		assertEquals(response,userLogin);
	}
	
	
}
