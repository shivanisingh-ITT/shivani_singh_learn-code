package serverTest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.google.gson.Gson;

import communicationProtocol.IMQRequest;
import server.ClientRequest;

public class ClientRequestTest {

	@Test
	public void getMessageFromClientTest() {
		ClientRequest clientRequest=new ClientRequest();
		 String request="{ response : message , data : message , dataFormat : JSON , version : 1.0 , destinationUrl : localhost }";
		 request=request.replace(' ', '"');
		 IMQRequest imqRequest=ClientRequest.getMessageFromClient(request);
	     assertEquals(imqRequest.getData(), "message");	  
	 }
}
  