package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import jdbcconnection.CreateStatementObject;
import model.Client;
import model.IMQMessage;
import model.Topic;
import properties.PropertyFile;

public class MessageDao {
	 Selector selector=new Selector();
	 
	 public String pushMessage(IMQMessage message,Topic topic,Client client) {
	  CreateStatementObject createStatement = new CreateStatementObject();
		PreparedStatement preparedStatement = null;
	     preparedStatement = createStatement
				.getPreparedStatement(selector.insertMessage());
		try {
			preparedStatement.setString(1, message.getData());
			preparedStatement.setString(2, topic.getTopicName());
			preparedStatement.setString(3, client.getClientName());
		  preparedStatement.execute(); 
		  return "Message published !!";
		} catch (SQLException exception) {
			return "SQL Error while publishing message ";
		}
	}
	
	 public ArrayList<IMQMessage> getMessages(Topic topic,Client client) {
		  CreateStatementObject createStatement = new CreateStatementObject();
			PreparedStatement preparedStatement = null;
		     preparedStatement = createStatement
					.getPreparedStatement(selector.getMessage());
			try { 
				preparedStatement.setString(1, topic.getTopicName()); 
				preparedStatement.setString(2, client.getClientName()); 
			    ResultSet result = preparedStatement.executeQuery();
			ArrayList<IMQMessage> messagesList = new ArrayList<IMQMessage>();
					while (result.next()) {
						IMQMessage message=new IMQMessage();
						message.setData(result.getString("data"));
						messagesList.add(message); 
					}
					return messagesList;
					
			} catch (SQLException exception) {
				exception.printStackTrace();
			}
			return null;
		} 
}
