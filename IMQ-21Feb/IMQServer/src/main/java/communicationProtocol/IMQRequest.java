package communicationProtocol;

public class IMQRequest extends IMQProtocol {
	private UserCommand userCommand;

	public UserCommand getUserCommand() {
		return userCommand;
	}

	public void setUserCommand(UserCommand userCommand) {
		this.userCommand = userCommand;
	}

}
