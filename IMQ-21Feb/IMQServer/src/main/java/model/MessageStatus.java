package model;

public enum MessageStatus {
   READY, INFLIGHT, DELAYED, DEAD;
}
