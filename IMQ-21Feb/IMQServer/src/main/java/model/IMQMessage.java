package model;

public class IMQMessage {
	private String data;

	public void setData(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}

}
