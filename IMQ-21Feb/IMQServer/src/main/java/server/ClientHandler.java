package server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import communicationProtocol.IMQRequest;
import communicationProtocol.IMQResponse;
import communicationProtocol.UserCommand;
import dao.ClientDao;
import dao.TopicDao;
import model.Client;
import model.IMQMessage;
import model.MessageStatus;
import model.Topic;
import publisherSubscriber.PublisherService;
import publisherSubscriber.SubscriberService;
import publisherSubscriber.TopicService;

public class ClientHandler extends Thread {
	private String bufferInput = null;
	Socket socket; 
	IMQRequest request = null;
	BufferedReader bufferReader = null;
	PrintWriter printWriter = null;

	TopicService topicService = new TopicService();
	PublisherService publisherService = new PublisherService();
	SubscriberService subscriberService = new SubscriberService();

	public ClientHandler(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {
		 String responseForClient;
		Client newClient=new Client(); 
         try {
        	bufferReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			printWriter = new PrintWriter(socket.getOutputStream());
			bufferInput = bufferReader.readLine();
			User user = new User();
			newClient.setClientName(user.login("shivani"));     
			request = ClientRequest.getMessageFromClient(bufferInput); 
			while (!request.getData().equalsIgnoreCase("exit")) { 
				
				Topic topic = new Topic();
				IMQMessage message = new IMQMessage();
				UserCommand command = request.getUserCommand();  
				switch (command) {
				case CONNECT:
					topic.setTopicName("Sports");
					responseForClient=topicService.connectTopic(newClient, topic);
					break;

				case PUBLISH:
					topic.setTopicName("Business");
					message.setData("imq new message"); 
					responseForClient=publisherService.pushMessage(newClient, topic, message);

					break;
				case CONFIG:
					topic.setTopicName("Business");
					responseForClient=subscriberService.getMessage(newClient, topic);
					break;
				case DISCONNECT:
					topic.setTopicName("Sports");
					responseForClient=topicService.disconnectTopic(newClient, topic);
					break;
				default:
					responseForClient="Invalid input,Please Try Again!!";
					break;
				}

				IMQResponse response =  ServerResponse.generateResponse(responseForClient);
				printWriter.println(ServerResponse.getServerResponse(response));
				printWriter.flush();
				bufferInput = bufferReader.readLine(); 
				request = ClientRequest.getMessageFromClient(bufferInput);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				System.out.println("Connection Closing..");
				if (bufferReader != null) {
					bufferReader.close();

				}

				if (printWriter != null) {
					printWriter.close();
				}
				if (socket != null) {
					socket.close();
				}

			} catch (IOException ioException) {
				System.out.println("Unable to close socket");

			}
		}
	}
}
