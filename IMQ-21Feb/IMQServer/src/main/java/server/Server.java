package server; 
import java.io.*;
import java.net.*;
import java.util.LinkedList;
import java.util.Queue;

import model.IMQMessage;
 
public class Server { 
	public void configureNewClient() throws IOException { 
		ServerSocket serverSocket = new ServerSocket(5056);
		while (true) {
			Socket socket = null;
			try {
				socket = serverSocket.accept(); 
               Queue<IMQMessage> messageQueue=new LinkedList<IMQMessage>();
			 	QueueHandler queueHandler=new QueueHandler(messageQueue);
				Thread thread = new ClientHandler(socket);
				thread.start();

			} catch (Exception e) {
				socket.close();
				e.printStackTrace();
			} 
		}
	}

	public static void main(String[] args) throws IOException {
		Server server = new Server();
		server.configureNewClient();
	}
}
