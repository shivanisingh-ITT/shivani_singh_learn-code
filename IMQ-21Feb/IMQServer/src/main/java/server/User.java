package server;
 
import java.io.IOException; 
import java.util.ArrayList; 
import dao.ClientDao;
import model.Client;

public class User {
	public String login(String clientName) throws IOException {
		Client newClient = new Client(); 
	//	printWriter.println("LOG IN \n"
	//			+ "Enter your username");
		//String clientName = "shivani"; //bufferReader.readLine();
		newClient.setClientName(clientName); 
		Boolean isClientExist = isClientExists(clientName);
		if (isClientExist) {
			 return "Welcome Back "+ clientName + "!! You have logged in sucessfully";
		} else {
			ClientDao newUser = new ClientDao();
			newUser.storeClient(newClient);
			return "Welcome "+ clientName + "!! You have signed up sucessfully";
		} 
	}

	private Boolean isClientExists(String clientName) {
		ClientDao clients = new ClientDao();
		ArrayList<String> allClients = clients.getAllClients();
		if (allClients.contains(clientName)) {
			return true;
		}
		return false;
	}
}
