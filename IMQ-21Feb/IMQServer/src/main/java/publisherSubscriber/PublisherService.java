package publisherSubscriber;

import java.io.IOException;  

import dao.MessageDao;
import dao.TopicDao;
import model.Client;
import model.IMQMessage;
import model.Topic;

public class PublisherService {
	TopicDao topicData = new TopicDao();
	MessageDao messageData=new MessageDao();
	
	public String pushMessage(Client client, Topic topic, IMQMessage message) throws IOException {
	    int topicCode=topicData.isTopicConnected(client, topic);
		if(topicCode==11) {   
			String response=messageData.pushMessage(message, topic, client);
			return response;
		}else if(topicCode==10) {   
			return "Please connect to Topic first";
		}else {    
			if(topicData.isTopicExists(topic)) {
				return "Please connect to Topic first";
			}else {
				return "Incorrect Topic Name "+ topic.getTopicName();
			}
		}
	} 	 
}
